#Configuration files for zsh

**This project contains various configuration files for zsh shell and related information**

The configuration is made on my local machine for personal use, however, it can be used by anyone.
It is fine tuned to my personal liking and most of the changes are very mimal from the default options.
Nonetheless, I suggest you to read carefully and use it at your own risk should you decide to adopt it.
